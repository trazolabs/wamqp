# Wamqp

A Wrapper for AMQP made for Trazo Labs use cases.

## Create a client

```go
client := wamqp.New("<URI>") // see https://www.rabbitmq.com/uri-spec.html
```

## Publish a message

```go
type Message struct {
    Name string
}

err := client.PublishStruct(&Message{"A name"})

// do something with the error.
```

## Subscribe for messages

```go
go func() {
    err := client.SubscribeStruct("MyServiceName", &Message{}, func (data []byte) error {
        // unmarshal data.
    })
    
    fmt.Println("Message", err)
}
```
