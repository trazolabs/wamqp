package wamqp

import (
	"fmt"
	"reflect"
)

type Handler struct {
	argType reflect.Type
	handler reflect.Value
	name    string
}

func NewHandler(s interface{}) (*Handler, error) {
	handlerType := reflect.TypeOf(s)

	if handlerType.Kind() != reflect.Func {
		return nil, fmt.Errorf("handler is not of kind func")
	}

	if handlerType.NumIn() != 1 {
		return nil, fmt.Errorf("handler must receive exactly one value")
	}

	if handlerType.NumOut() != 1 {
		return nil, fmt.Errorf("handler must return exactly one value")
	}

	returnType := handlerType.Out(0)
	if !returnType.Implements(reflect.TypeOf((*error)(nil)).Elem()) {
		return nil, fmt.Errorf("handler must return an error")
	}

	argType := handlerType.In(0)
	if argType.Kind() != reflect.Ptr {
		return nil, fmt.Errorf("handler value must be of a pointer")
	}
	argType = argType.Elem()

	return &Handler{
		argType: argType,
		handler: reflect.ValueOf(s),
		name:    argType.Name(),
	}, nil
}

func (h *Handler) Call(value interface{}) error {
	if reflect.TypeOf(value).Elem() != h.argType {
		return fmt.Errorf("handler called with incompatible type")
	}

	returnValues := h.handler.Call([]reflect.Value{reflect.ValueOf(value)})
	possibleErr := returnValues[0]
	if possibleErr.IsNil() {
		return nil
	}
	return possibleErr.Interface().(error)
}

func (h *Handler) Value() interface{} {
	return reflect.New(h.argType).Interface()
}
