package wamqp

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"github.com/jpillora/backoff"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

const ExchangeName = "amq.direct"

type Client struct {
	uri string

	publishChannel *amqp.Channel
}

// New client with URI, as defined in https://www.rabbitmq.com/uri-spec.html
// Example: amqp://user:pass@host:1234/
func New(uri string) *Client {
	return &Client{uri: uri}
}

// Connect will try several connections to the rabbitmq instance,
// using a backoff to delay each connection attempt.
// Set limit to false to indicate if this connection should retry an
// undefined amount of times.
func (s *Client) Connect(limit bool) (*amqp.Channel, error) {
	b := &backoff.Backoff{
		Min:    100 * time.Millisecond,
		Max:    5 * time.Second,
		Factor: 1.5,
		Jitter: false,
	}

	now := time.Now()

	for {
		logrus.Debug("connecting to rabbitmq...")

		channel, err := s.createChannel()
		if err != nil {
			if limit == false || time.Now().Sub(now).Seconds() < 20 {
				duration := b.Duration()
				logrus.WithField("debounce", duration).Warn("connection with rabbitmq failed, trying again")
				time.Sleep(duration)
			} else {
				logrus.Error("connection with rabbitmq failed")
				return nil, amqp.ErrClosed
			}
		} else {
			logrus.Debug("connection with rabbitmq established")
			return channel, nil
		}
	}
}

func (s *Client) PublishStruct(event interface{}) error {
	return s.Publish(structName(event), event)
}

func (s *Client) Publish(event string, body interface{}) error {
	// marshal data into bytes.
	data, err := json.Marshal(body)
	if err != nil {
		return err
	}

	// check if we have a publish channel created.
	if err := s.initPublishChannel(); err != nil {
		return err
	}

	for {
		// try to publish the message on the exchange.
		err = s.publishChannel.Publish(ExchangeName, event, false, false, amqp.Publishing{
			ContentType:  "application/json",
			Body:         data,
			DeliveryMode: amqp.Persistent,
		})

		if err != nil {
			// if an error occurred, we'll try to establish a new connection and retry.
			if errors.Is(err, amqp.ErrClosed) {
				if err := s.createPublishChannel(); err != nil {
					return err
				}
				continue
			}

			return err
		} else {
			break
		}
	}

	logrus.WithFields(logrus.Fields{
		"event": event,
		"data":  string(data),
	}).Debug("event published to bus")

	return nil
}

func (s *Client) initPublishChannel() error {
	if s.publishChannel == nil {
		return s.createPublishChannel()
	}
	return nil
}

func (s *Client) createPublishChannel() error {
	if publishChannel, err := s.Connect(true); err != nil {
		return err
	} else {
		s.publishChannel = publishChannel
	}

	return nil
}

// SubscribeStruct subscribes a struct to a channel. Each message will be passed to the
// handler function as they arrive. If the connection is dropped this function will
// automatically retry to establish the connection.
//
// Subscriptions with different consumer names will all receive a given message. But
// subscriptions with the same consumer name, only one of them will receive a message.
//
// Note: the queue name will be composed of the service name, and the event name.
func (s *Client) SubscribeStruct(consumer string, h interface{}) error {
	handler, err := NewHandler(h)
	if err != nil {
		return err
	}

	return s.Subscribe(consumer, handler.name, func(data []byte) error {
		value := handler.Value()
		if err := json.Unmarshal(data, &value); err != nil {
			return err
		}
		return handler.Call(value)
	})
}

// Subscribe to a channel. Each message will be passed to the handler function as they arrive.
// If the connection is dropped this function will automatically retry to establish the connection.
// Note: this function fails silently.
func (s *Client) Subscribe(service, queue string, handler func(data []byte) error) error {
	queueName := fmt.Sprintf("%s.%s", service, queue)

	for {
		channel, err := s.Connect(false)
		if err != nil {
			return err
		}

		q, err := channel.QueueDeclare(queueName, true, false, false, false, nil)
		if err != nil {
			return err
		}

		if err := channel.QueueBind(q.Name, queue, ExchangeName, false, nil); err != nil {
			return err
		}

		messages, err := channel.Consume(q.Name, "", false, false, false, false, nil)
		if err != nil {
			return err
		}

		logrus.WithField("queue", queue).Debug("subscribed to queue")

		for message := range messages {
			start := time.Now()
			if err := handler(message.Body); err != nil {
				logrus.WithFields(logrus.Fields{"queue": queue}).WithError(err).Error("queue message error")
				continue
			}

			if err := message.Ack(false); err != nil {
				logrus.WithFields(logrus.Fields{
					"queue":   queue,
					"elapsed": time.Since(start) / time.Millisecond,
				}).Debug("message received from queue but not acknowledged")
				continue
			}

			logrus.WithFields(logrus.Fields{
				"queue":   queue,
				"elapsed": time.Since(start) / time.Millisecond,
			}).Debug("message received from queue")
		}

		logrus.WithField("queue", queue).Warn("connection with rabbitmq dropped")
	}
}

func (s *Client) createChannel() (*amqp.Channel, error) {
	conn, err := amqp.Dial(s.uri)
	defer conn.Close()
	if err != nil {
		return nil, err
	}
	return conn.Channel()
}
