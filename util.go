package wamqp

import (
	"reflect"
)

func structName(m interface{}) string {
	if reflect.TypeOf(m).Kind() == reflect.Ptr {
		m = reflect.ValueOf(m).Elem().Interface()
	}
	return reflect.TypeOf(m).Name()
}
