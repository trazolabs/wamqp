module gitlab.com/trazolabs/wamqp

go 1.15

require (
	github.com/jpillora/backoff v1.0.0
	github.com/sirupsen/logrus v1.8.0
	github.com/streadway/amqp v1.0.0
)
