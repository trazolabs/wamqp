package main

import (
	"fmt"
	"time"

	"gitlab.com/trazolabs/wamqp"
)

type MyEvent struct {
	Value string
}

func main() {
	client := wamqp.New("amqp://guest:guest@localhost:5672")

	go func() {
		time.Sleep(time.Second * 1)

		fmt.Println("SEND")
		err := client.PublishStruct(MyEvent{Value: "world"}, wamqp.WithDelay(5000))
		if err != nil {
			panic(err)
		}
	}()

	go func() {
		if err := client.SubscribeStruct("Client1", func(event *MyEvent) error {
			fmt.Println("Client1.A", "hello,", event.Value)
			return nil
		}); err != nil {
			panic(err)
		}
	}()

	go func() {
		if err := client.SubscribeStruct("Client1", func(event *MyEvent) error {
			fmt.Println("Client1.B", "hello,", event.Value)
			return nil
		}); err != nil {
			panic(err)
		}
	}()

	if err := client.SubscribeStruct("Client2", func(event *MyEvent) error {
		fmt.Println("Client2", "hello,", event.Value)
		return nil
	}); err != nil {
		panic(err)
	}
}
